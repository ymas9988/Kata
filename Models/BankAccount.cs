using Newtonsoft.Json;

public class BankAccount
{
    public int Id { get; set; }
    public decimal Balance { get; set; }

    // [JsonIgnore]
    // public List<Transaction> Transactions {get; set;}
}

public class Transaction
{
    public int Id { get; set; }
    public int BankAccountId { get; set; }
    public decimal Amount { get; set; }
    public string Type { get; set; }
}
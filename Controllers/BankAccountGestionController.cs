using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

[Route("api/gestion")]
[ApiController]

public class BankAccountGestionController: ControllerBase
{
    private readonly BankAccountGestionService _bankAccountGestionService;

        public BankAccountGestionController(BankAccountGestionService bankAccountGestionService)
        {
            _bankAccountGestionService = bankAccountGestionService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<BankAccount>> GetAll()
        {
            return Ok(_bankAccountGestionService.GetAll());
        }

        [HttpGet("{id}", Name = "GetBankAccount")]
        public ActionResult<BankAccount> GetById(int id)
        {
            var item = _bankAccountGestionService.GetById(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult Create(BankAccount item)
        {
            _bankAccountGestionService.Create(item);
            return CreatedAtRoute("GetBankAccount", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, BankAccount item)
        {
            var bankAccount = _bankAccountGestionService.GetById(id);
            if (bankAccount == null)
            {
                return NotFound();
            }

            _bankAccountGestionService.Update(id, item);
            return NoContent();
        }

        [HttpDelete("{id}")]
         public IActionResult Delete(int id)
        {
            var bankAccount = _bankAccountGestionService.GetById(id);
            if (bankAccount == null)
            {
                return NotFound();
            }

            _bankAccountGestionService.Delete(id);
            return NoContent();
        }
 }


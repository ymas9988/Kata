
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.EntityFrameworkCore;


[Route("api/bankaccounts")]
[ApiController]
public class BankAccountsController : ControllerBase
{
    private readonly BankAccountService _service;
   public BankAccountsController(BankAccountService service)
   {
       _service = service;
   }

    [HttpGet]
    public ActionResult<IEnumerable<BankAccount>> Get()
    {
    var bankAccounts = _service.GetAll();
    return Ok(bankAccounts);
    }


   [HttpGet("{id}")]
   public ActionResult<BankAccount> Get(int id)
   {
       var bankAccount = _service.Get(id);
       if (bankAccount == null)
           return NotFound();

       return bankAccount;
   }

//    [HttpGet("{id}/transactions")]
//    public ActionResult<IEnumerable<Transaction>> GetTransactions(int id)
//    {
//        var transactions = _service.GetTransactions(id);
//        return Ok(transactions);
//    }

   [HttpPost("{id}/deposit/{amount}")]
   public ActionResult<Transaction> Deposit(int id,  decimal amount)
   {
       var transaction = _service.Deposit(id, amount);
       if (transaction == null)
           return BadRequest();

       return transaction;
   }

   [HttpPost("{id}/withdraw/{amount}")]
   public ActionResult<Transaction> Withdraw(int id,  decimal amount)
   {
       var transaction = _service.Withdraw(id, amount);
       if (transaction == null)
           return BadRequest();

       return transaction;
   }
}
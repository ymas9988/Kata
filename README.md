clone le repo

docker-compose up -d 

nb. le container api va sans doute cracher, il faudra le relancer car il attend que la base de donnée soit prête (pas eu le temps de fixer ça) 


crée un compte avec l'adresse [POST]: https://localhost:8080/api/gestion/ avec body { "item" : {}} et un compte sera créer avec un id qui s'incremente


pour afficher la liste des comptes [GET] : https://localhost:8080/api/bankaccounts/


pour faire un dépot [POST] : https://localhost:8080/api/bankaccounts/{id}/deposit/{amount}


pour faire un retrait [POST] : https://localhost:8080/api/bankaccounts/{id}/withdraw/{amount}


Nb : vous pouvez utiliser l'ui swagger pour tester l'api à l'adresse suivante : http://localhost:8080/swagger/index.html
lien pour phpmyadmin : http://localhost:8081/

nb. la gestion des cas tordu n'est pas optimale
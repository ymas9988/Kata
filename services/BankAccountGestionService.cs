using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
    public class BankAccountGestionService 
    {
        private readonly AppDbContext _context;

        public BankAccountGestionService(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<BankAccount> GetAll()
        {
            return _context.BankAccounts;
        }

        public BankAccount GetById(int id)
        {
            return _context.BankAccounts.Find(id);
        }

        public void Create(BankAccount item)
        {
            _context.BankAccounts.Add(item);
            _context.SaveChanges();
        }

        public void Update(int id, BankAccount item)
        {
            var bankAccount = _context.BankAccounts.Find(id);
            if (bankAccount == null)
            {
                return;
            }

            bankAccount.Balance = item.Balance;

            _context.BankAccounts.Update(bankAccount);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var bankAccount = _context.BankAccounts.Find(id);
            if (bankAccount == null)
            {
                return;
            }

            _context.BankAccounts.Remove(bankAccount);
            _context.SaveChanges();
        }
    }


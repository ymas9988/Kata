using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;
using Microsoft.EntityFrameworkCore.Design;


public class BankAccountService
{
    private readonly AppDbContext _context;

    public BankAccountService(AppDbContext context)
    {
        _context = context;
    }

    public BankAccount Get(int id)
    {
        return _context.BankAccounts.FirstOrDefault(b => b.Id == id);
    }

    // public IEnumerable<Transaction> GetTransactions(int id)
    // {
    //     var bankAccount = _context.BankAccounts.Include(b => b.Transactions)
    //         .FirstOrDefault(b => b.Id == id);
    //     if (bankAccount == null)
    //         return null;

    //     return bankAccount.Transactions;
    // }

    public Transaction Deposit(int id, decimal amount)
    {
        var bankAccount = _context.BankAccounts.FirstOrDefault(b => b.Id == id);
        if (bankAccount == null)
            return null;

        if (amount <= 0)
            throw new ArgumentException("Amount cannot be negative.");

        var transaction = new Transaction
        {
            Amount = amount,
            Type = "Deposit"
        };

       // bankAccount.Transactions.Add(transaction);
        bankAccount.Balance += amount;
        _context.SaveChanges();

        return transaction;
    }

    public Transaction Withdraw(int id, decimal amount)
    {
        var bankAccount = _context.BankAccounts.FirstOrDefault(b => b.Id == id);
        if (bankAccount == null)
            return null;

        if (amount <= 0)
            throw new ArgumentException("Amount cannot be negative.");

        if (bankAccount.Balance < amount)
            throw new ArgumentException("You don't have enough money");


        var transaction = new Transaction
               {
               Amount = amount,
               Type = "Withdraw"
           };

           //bankAccount.Transactions.Add(transaction);
           bankAccount.Balance -= amount;
           _context.SaveChanges();

           return transaction;
       }

       public IEnumerable<BankAccount> GetAll()
       {
           return _context.BankAccounts.ToList();
       }
   }
  
